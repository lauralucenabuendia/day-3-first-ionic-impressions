import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rating-page',
  templateUrl: './rating-page.page.html',
  styleUrls: ['./rating-page.page.scss'],
})
export class RatingPage implements OnInit {
  // Variables
  public positivePuntuation: number;
  public badPuntuation: number;
  public averageScore: string;
  public displayedInfo: boolean;
  public sexList: string[];

  private totalScore: number;
  private numberVotes: number;

  //-------------------------------
  // Ciclo de vida de la página
  //-------------------------------
  constructor() { }

  ngOnInit(): void {
    this.sexList = ['Masculino', 'Femenino'];
    this.resetPage();

  }

  //-------------------------------
  // Métodos de la página
  //-------------------------------

  // Nos añade una buena puntuación '10'
  addPositivePuntuation(){
    this.positivePuntuation += 1;
    this.totalScore += 10;
    this.numberVotes++;

    this.calculateAveragScore();
  }

  // No nos añade buena puntuación, pero se le suma una puntuación con valor '0'
  addBadPuntuation(){
    this.badPuntuation += 1;
    this.numberVotes++;

    this.calculateAveragScore();
  }

  // Nos calcula la puntuación media
  calculateAveragScore(){
    this.averageScore = (this.totalScore / this.numberVotes).toFixed(2);
  }


  // Toogle para cambiar la visualización de la descripción
  showInformationToggle(){
    this.displayedInfo = !this.displayedInfo;
  }


  // Nos resetea los valores/variables de la página
  resetPage(){
    this.displayedInfo = false;
    this.positivePuntuation = 0;
    this.badPuntuation = 0;
    this.totalScore = 0;
    this.averageScore = '0.0';
    this.numberVotes = 0;
  }
}
