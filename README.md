# Day-3-First-Ionic-Impressions

First contact with an Ionic-App
_______________________________________

<b>Primera versión de la app.</b>

Aquí absolutamente todo se realiza dentro de una misma página/componente.

En éste mismo repositorio también encontramos varias versines donde desglosamos la página en componentes y trabajaremos la comunicación entre éstos; ya sea a través de Inputs/Outputs o Servicios.

Descripción de las diferentes ramas:

<b>-develop:</b> Aquí es dónde hemos estado desarrollando las diferentes versiones de la app (_Se ha eliminado debido a que hemos realizado un 'merge' y ya ha quedado vacía_).

<b>-main:</b> Ejercicio realizado completo, todo desarrollado en una misma página.

<b>-app-with-input-output:</b> Ejercicio realizado completo. División de la página realizada en diferentes componentes; estos a su vez se comunican a través de Inputs/Outputs.

<b>-app-with-services:</b> Ejercicio realizado completo. División de la página realizada en diferentes componentes; estos se comunican a través de un servicio, para así poder evitar una cantidad abrumadora de Inputs/Ouputs, es la manera más eficiente y correcta de comunicar componentes.

